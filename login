<html>
<head>
<title>{title}</title>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="style.css"/>
<script type="text/javascript">
  function reload() {
    var Lang = document.login.lang.value;
    document.cookie="Lang=" + Lang;
    location.assign(window.location);
  }
  </script>
</head>
<body>
<form name="login" method="post" action="index.php">
<table>
  <tr>
    <td colspan="2">
      <div align="right">
	<select name="lang" style="width:80" onchange="reload()">
	  <option value="ru">Русский</option>
	  <option value="eng">English</option>
	</select>
      </div>
      <div class="center">{echo}</div>
    </td>
  </tr>
  <tr>
    <td>{contacts}</td>
    <td><input name="contacts" type="text" size="20"/></td>
  </tr>
  <tr>
    <td>{password}</td>
    <td><input name="password" type="password" size="20"/></td>
  </tr>
  <tr>
    <td><a href="reg.php">{reg}</a></td>
    <td>
      <input name="submit" type="submit" value="{submit}"/> 
    </td>
  </tr>
</table>
</form>
</body>
</html> 
