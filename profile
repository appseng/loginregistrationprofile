<html>
<head>
<title>{title}</title>
<meta charset="UTF-8"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<form name="login" method="post" action="profile.php">
<table class="profile">
  <tr>
    <td colspan="2"><a href="profile.php?exit=1">{loginref}</a></td>
  </tr>
  <tr>
    <td>{firstname}</td>
    <td>{firstnamevalue}</td>
  </tr>
  <tr>
    <td>{middlename}</td>
    <td>{middlenamevalue}</td>
  </tr>
  <tr>
    <td>{lastname}</td>
    <td>{lastnamevalue}</td>
  </tr>
  <tr>
    <td>{contacts}</td>
    <td>{contactsvalue}</td>
  </tr>
  <tr>
    <td>{yearofbirth}</td>
    <td>{yearofbirthvalue}</td>
  </tr>
  <tr>
    <td>{location}</td>
    <td>{locationvalue}</td>
  </tr>
  <tr>
    <td>{married}</td>
    <td>{marriedvalue}</td>
  </tr>
  <tr>
    <td>{education}</td>
    <td>{educationvalue}</td>
  </tr>
  <tr>
    <td>{extrainfo}</td>
    <td>{extrainfovalue}</td>
  </tr>
  <tr>
    <td>{userimage}</td>
    <td><img src="image.php?id={imageid}" alt="{altimage}"/></td>
  </tr>  <tr><td colspan="2">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html> 
