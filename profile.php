<?php
// checking if there is need to redirect to login page
if(!isset($_COOKIE['ref']) || isset($_GET['exit'])) {
  setcookie("ref", "");
  setcookie("contacts","");
  setcookie("password","");
  header('Location: index.php');
}
setcookie("ref", "profile");
// checking e-mail and password of the user
$contacts = isset($_COOKIE['contacts']) ? $_COOKIE['contacts'] : '';
$password = isset($_COOKIE['password']) ? $_COOKIE['password'] : '';
if ($contacts == '' || $password == '')
  header('Location: index.php');

// connecting to the database
include "dbinfo.php";
$link = connect();
// checking the user's info
$sql = "SELECT * from $db_table where contacts = '$contacts' and password = '$password'";
$q = mysql_query($sql) or die(mysql_error());
$rows = mysql_num_rows($q);

// checking if there is such a user
if ($rows > 0) {
  // loading language data
  $lang = isset($_COOKIE['Lang']) ? $_COOKIE['Lang'] : 'ru';
  if ($lang == "eng")
    include 'eng.php';
  else
    include 'rus.php';
  
  // fill in the form with labels basing on info from the language file
  $html = file_get_contents("profile");
  $html = str_replace("{loginref}", $loginref, $html);
  $html = str_replace("{title}", $ptitle, $html);
  $html = str_replace("{contacts}", $contacts, $html);
  $html = str_replace("{firstname}", $firstname, $html);
  $html = str_replace("{middlename}", $middlename, $html);
  $html = str_replace("{lastname}", $lastname, $html);
  $html = str_replace("{yearofbirth}", $yearofbirth, $html);
  $html = str_replace("{location}", $location, $html);
  $html = str_replace("{married}", $married, $html);
  $html = str_replace("{education}", $education, $html);
  $html = str_replace("{extrainfo}", $extrainfo, $html);
  $html = str_replace("{userimage}", $userimage, $html);
  $html = str_replace("{altimage}", $altimage, $html);

  $f = mysql_fetch_array($q);
  // fill in the form with labels basing on data from database
  $html = str_replace("{imageid}", $f['id'], $html);
  $html = str_replace("{contactsvalue}", $f['contacts'], $html);
  $html = str_replace("{firstnamevalue}", $f['firstname'], $html);
  $html = str_replace("{middlenamevalue}", $f['middlename'], $html);
  $html = str_replace("{lastnamevalue}", $f['lastname'], $html);
  $html = str_replace("{yearofbirthvalue}", $f['yearofbirth'], $html);
  $html = str_replace("{locationvalue}", $f['location'], $html);
  $married = ($f['married'] == 1) ? $yes : $no;    
  $html = str_replace("{marriedvalue}", $married, $html);
  $html = str_replace("{educationvalue}", $f['education'], $html);
  $html = str_replace("{extrainfovalue}", $f['extrainfo'], $html);
  mysql_close($link);
  
  echo $html;  
}
else {// if the user hasn't registered
  mysql_close($link);
  header("Location: index.php");
  }
?> 
