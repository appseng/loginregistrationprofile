<?php
if ( isset( $_GET['id'] ) ) {
  $id = (int)$_GET['id'];
  if ( $id > 0 ) {
    header('Content-Type: image/jpeg');
    $image = "images/$id.jpg";
    if (file_exists($image)) {
      list($width, $height) = getimagesize($image);
      $new_width = 150;
      $new_height = $height*$new_width/$width;
      $image_p = imagecreatetruecolor($new_width, $new_height);
      $image = imagecreatefromjpeg($image);
      imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
      imagejpeg($image_p, null, 100);
    }
  }
}
?> 
