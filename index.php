<?php
function showInit($err) {
  // language detection and loading a language file
  $lang = isset($_COOKIE['Lang']) ? $_COOKIE['Lang'] : 'ru';
  $formlang = isset($_POST['lang']) ? $_POST['lang'] : 'ru';
  if (isset($_POST['submit']) && $lang != $formlang)
    $lang = $formlang;

  if ($lang == "eng")
    include 'eng.php';
  else
    include 'rus.php';
  
  // setting language
  setcookie("Lang", $lang);
  
  // fill in the form with labels basing on info from language file
  $html = file_get_contents("login");
  $html = str_replace("{reset}", $reset, $html);
  $html = str_replace("{submit}", $submit, $html);
  $html = str_replace("{title}", $ititle, $html);
  $html = str_replace("{contacts}", $contacts, $html);
  $html = str_replace("{password}", $password, $html);
  $html = str_replace("{reg}", $reg, $html);
  
  // configure rihgt language
  if ($lang == "eng")
    $html = str_replace("<option value=\"eng\">", "<option selected value=\"eng\">", $html);
  else
    $html = str_replace("<option value=\"ru\">", "<option selected value=\"ru\">", $html);

  // errors display
  if ($err)
    $html = str_replace("{echo}", $echo, $html); 
  else
    $html = str_replace("{echo}", "", $html); 

  echo $html;
}

if (isset($_POST['contacts']) && isset($_POST['password'])) {
  include "func.php";
  $contacts = check($_POST['contacts']);
  $password = check($_POST['password']);
  
  // checking if such a user exists
  include "dbinfo.php";
  $link = connect();  
  $sql = "SELECT * from $db_table where contacts = \"$contacts\" and password = \"$password\"";
  $q = mysql_query($sql) or die(mysql_error());
  $rows = mysql_num_rows($q);
  
  // if the user has already existed redirect to profile page
  if ($rows > 0) {
    $f = mysql_fetch_array($q);
    setcookie("ref", "index");
    setcookie("contacts",$contacts);
    setcookie("password", $password);
    header("Location: profile.php");
  }
  else
    showInit(true);
  
  mysql_close($link);  
}
else // page is loading at first time or is being reloaded
  showInit(false);
?> 