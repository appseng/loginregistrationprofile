<?php
function showContent($validationerrors) {
  // language detection and loading a language file
  $lang = isset($_COOKIE['Lang']) ? $_COOKIE['Lang'] : 'ru';
  if ($lang == "eng")
    include 'eng.php';
  else
    include 'rus.php';

  // fill in the form with labels basing on info from language file
  $html = file_get_contents("reg");
  $html = str_replace("{title}", $rtitle, $html);
  $html = str_replace("{reset}", $reset, $html);
  $html = str_replace("{submit}", $submit, $html);
  $html = str_replace("{reg}", $reg, $html);
  $html = str_replace("{repeat}", $repeat, $html);  
  $html = str_replace("{contacts}", $contacts, $html);
  $html = str_replace("{password}", $password, $html);
  $html = str_replace("{firstname}", $firstname, $html);
  $html = str_replace("{middlename}", $middlename, $html);
  $html = str_replace("{lastname}", $lastname, $html);
  $html = str_replace("{yearofbirth}", $yearofbirth, $html);
  $html = str_replace("{location}", $location, $html);
  if ($married == 1)
    $html = str_replace("<option value=\"yes\">", "<option selected value=\"yes\">", $html);
  else
    $html = str_replace("<option value=\"no\">", "<option selected value=\"no\">", $html);

  $html = str_replace("{married}", $married, $html);
  $html = str_replace("{yes}", $yes, $html);
  $html = str_replace("{no}", $no, $html);  
  $html = str_replace("{education}", $education, $html);
  $html = str_replace("{extrainfo}", $extrainfo, $html);
  $html = str_replace("{userimage}", $userimage, $html);
  $html = str_replace("{altimage}", $altimage, $html);
  
  // display errors
  $errors = "";
  if($validationerrors != null) {
    if(isset($validationerrors["password"])) {
      $html = str_replace("{valpassword}", "*", $html);
      $errors .= $passworderr . "<br>";
    }
    else 
      $html = str_replace("{valpassword}", "", $html);
      
    if (isset($validationerrors["repeat"])) {
      $html = str_replace("{valrepeat}", "*", $html);
      $errors .= $repeaterr . "<br>";
    }
    else
      $html = str_replace("{valrepeat}", "", $html);
      
    if (isset($validationerrors["contacts"])) {
      $html = str_replace("{valcontacts}", "*", $html);
      switch($validationerrors["contacts"]) {
      case 1:
	$errors .= $contactserr1 . "<br>";
	break;
      case 2:
	$errors .= $contactserr2 . "<br>";
	break;
      }
    }
    else
      $html = str_replace("{valcontacts}", "", $html);
      
   if (isset($validationerrors["yearofbirth"])) {
      $html = str_replace("{valyearofbirth}", "*", $html);
      $errors .= $yearofbirtherr . "<br>";
   }
   else
      $html = str_replace("{valyearofbirth}", "", $html);
  }
  else {
      $html = str_replace("{valyearofbirth}", "", $html);
      $html = str_replace("{valpassword}", "", $html);
      $html = str_replace("{valrepeat}", "", $html);
      $html = str_replace("{valcontacts}", "", $html);
  }
  $html = str_replace("{validationerrors}", $errors, $html);
  
  echo $html;
}

// chech if there was submit
if (isset($_POST['submit'])) {
  include "dbinfo.php";
  $link = connect();
  include "func.php";
  $contacts = check($_POST['contacts']);
  $password = check($_POST['password']);
  $repeat = check($_POST['repeat']);
  $firstname = check($_POST['firstname']);
  $middlename = check($_POST['middlename']);
  $lastname = check($_POST['lastname']);
  $yearofbirth = check($_POST['yearofbirth']);
  $yearofbirthlength = strlen($yearofbirth);
  
  $location = check($_POST['location']);
  
  $married = check($_POST['married']);
  $married = ($married == "yes") ? 1 : 0;
  $education = check($_POST['education']);
  $extrainfo = check($_POST['extrainfo']);  
  
  /// validation of input datum.
  $validationerrors = array();
  // year validation
  if (!(is_numeric($yearofbirth) && 
    (($yearofbirthlength > 2 && $yearofbirth > 1900 && $yearofbirth < 2156) ||
    ($yearofbirthlength > 0 && $yearofbirthlength < 3))))
    $validationerrors["yearofbirth"] = 1;
  // password validation
  if ($password != $repeat)
    $validationerrors["repeat"] = 1;
  if ($password == "")
    $validationerrors["password"] = 1;
  // validation of e-mail
  $sql = "select * from $db_table where contacts = \"$contacts\"";
  $q = mysql_query($sql) or die(mysql_error());
  $rows = mysql_num_rows($q);
  if ($rows > 0)
    $validationerrors["contacts"] = 1;
  else {
    if (!filter_var($contacts, FILTER_VALIDATE_EMAIL))
      $validationerrors["contacts"] = 2;
  }
  
  // check if there were errors
  if (count($validationerrors) > 0) {
    mysql_close($link);
    showContent($validationerrors);
  }
  else {
  //
  /// insert userinfo in db and get new_id
  $sql = "insert into $db_table(firstname,middlename,lastname,yearofbirth,location,married,contacts,extrainfo,education,password) values('$firstname','$middlename','$lastname',$yearofbirth,'$location',$married,'$contacts','$extrainfo','$education','$password')";
  $q = mysql_query($sql) or die(mysql_error());
  $sql = "SELECT LAST_INSERT_ID() as 'id'";
  $q = mysql_query($sql) or die(mysql_error());
  
  // loading userimage on the server
  $tmp_name = $_FILES["userimage"]["tmp_name"];
  if (is_uploaded_file($tmp_name)) {
    if ( $_FILES['userimage']['error'] == 0 ) {
      if(substr($_FILES['userimage']['type'], 0, 5)=='image' ) {
	$imagetype = substr($_FILES['userimage']['type'], 6, 3);
	switch ($imagetype) {
	  case 'png':
	    $image = imagecreatefrompng($tmp_name);
	    break;
	  case 'gif':
	    $image = imagecreatefromgif($tmp_name);
	    break;
	  case 'jpe':
	    $image = imagecreatefromjpeg($tmp_name);
	    break;
	}
	$f = mysql_fetch_array($q);
	imagejpeg($image, "images/$f[id].jpg");
	imagedestroy($image);
      }
    }
  }
  mysql_close($link);
  
  /// setcookies and redirect
  setcookie("ref", "reg");
  setcookie("contacts",$contacts);
  setcookie("password", $password);
  header("Location: profile.php");
  }
}
else
  showContent(null);  
?>
