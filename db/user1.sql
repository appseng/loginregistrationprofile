-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 08 2015 г., 10:40
-- Версия сервера: 5.5.33-MariaDB
-- Версия PHP: 5.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `user1`
--

-- --------------------------------------------------------

--
-- Структура таблицы `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `yearofbirth` year(4) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `married` bit(1) DEFAULT NULL,
  `contacts` varchar(100) NOT NULL,
  `extrainfo` varchar(200) DEFAULT NULL,
  `education` varchar(100) DEFAULT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contacts` (`contacts`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='User''s info for login/registration form' AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `userinfo`
--

INSERT INTO `userinfo` (`id`, `firstname`, `middlename`, `lastname`, `yearofbirth`, `location`, `married`, `contacts`, `extrainfo`, `education`, `password`) VALUES
(2, 'Дмитрий', 'Сергеевич', 'Кузнецов', 1987, 'г. Елец', b'0', 'appseng@yandex.ru', 'php', 'высшее', 'appseng'),
(3, 'Марс', 'Филипович', '', 2000, 'диван', b'0', 'cat@microsoft.com', 'кот', 'нет', 'cat');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
