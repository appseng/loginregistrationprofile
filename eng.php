<?php
$contacts = "User's e-mail :";
$password = "User's password :";
// index.php
$reset = "Reset";
$submit = "Enter";
$reg = "Registration";
$ititle = "Wellcome!";
$echo = "Such a user doesn't exist.";

// reg.php
$rtitle = "Registration";
$repeat = "Repeat :"; 
$firstname = "First name :";
$middlename = "Middle name :";
$lastname = "Last name :";
$yearofbirth = "Year of birth :";
$location = "Location :";
$married = "Married :";
$education = "Education :";
$extrainfo = "Additional info :";
$yes = "Yes";
$no = "No";
$userimage = "Image :";
$passworderr = "Password is incorrect.";
$repeaterr = "Passwords are not equal.";
$contactserr1 = "E-mail is already registered.";
$contactserr2 = "E-mail is incorrect.";
$yearofbirtherr = "Year is out of range value.";

// profile.php
$ptitle = "User's profile info";
$loginref = "Exit";
$altimage = "No image";

?>